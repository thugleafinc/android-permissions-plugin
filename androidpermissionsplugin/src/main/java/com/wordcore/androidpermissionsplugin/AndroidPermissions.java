package com.wordcore.androidpermissionsplugin;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.unity3d.player.UnityPlayer;

/**
 * Created by Yassine on 12/07/2016.
 */
public class AndroidPermissions extends Fragment {

    public static final String TAG = AndroidPermissions.class.getSimpleName();

    public static AndroidPermissions instance;
    private static Activity unityActivity;
    private static int requestCode = 0;
    private static final String CALLBACK_METHOD_NAME = "PluginPermissionsGranted";
    private String gameObjectName;
    private Handler handler;

    public static void start(String gameObjectName) {
        unityActivity = UnityPlayer.currentActivity;
        instance = new AndroidPermissions();
        instance.gameObjectName = gameObjectName;
        unityActivity.getFragmentManager().beginTransaction().add(instance, AndroidPermissions.TAG).commit();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        handler = new Handler();
    }

    public void getAndroidScreenshotSharePermissions(int _requestCode)
    {
        //Log.d(TAG,"getAndroidScreenshotSharePermissions");
        requestCode = _requestCode;
        checkPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE});
    }

    public void getGalleryAccessPermissions(int _requestCode)
    {
        //Log.d(TAG,"getGalleryAccessPermissions");
        requestCode = _requestCode;
        checkPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE});

    }

    public void getCameraAccessPermissions(int _requestCode)
    {
        //Log.d(TAG,"getCameraAccessPermissions");
        requestCode = _requestCode;
        checkPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE});
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermissions(String[] permissions)
    {
        //Log.d(TAG,"checkPermissions");
        boolean shouldShowExplanation = false;
        int currentApiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentApiVersion >= android.os.Build.VERSION_CODES.M){
            for (String permission: permissions) {
                if (ContextCompat.checkSelfPermission(unityActivity, permission) !=
                        PackageManager.PERMISSION_GRANTED) {
                    if (shouldShowRequestPermissionRationale(permission)) {
                        // Check if user did NOT tick the "Never ask again" box before.
                        shouldShowExplanation = true;
                        break;
                    }
                }
            }
            if(shouldShowExplanation){
                showExplanationDialog(permissions);
            } else {
                // No explanation needed, we can request the permission.
                requestPermissions(permissions, requestCode);
            }
        } else{
            // Permissions are granted for api level < 23
            permissionsGranted();
        }

    }

    private void showExplanationDialog(final String[] permissions)
    {
        //Log.d(TAG,"showExplanationDialog");
        Runnable runnable = new Runnable(){
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(unityActivity);
                builder.setTitle("Permission denied");
                builder.setMessage("Certain permission must be granted to access this feature.\nLast time you denied these permissions.\nAre you sure you want to keep them unchanged?");
                builder.setPositiveButton("RE-TRY", new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.M)
                    public void onClick(DialogInterface dialog, int id) {
                        requestPermissions(permissions, requestCode);
                    }
                });
                builder.setNegativeButton("I'M SURE", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(unityActivity.getBaseContext(),"Permission denied", Toast.LENGTH_LONG).show();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        };
        handler.post(runnable);
    }


    @Override
    public void onRequestPermissionsResult(int code, String permissions[], int[] grantResults)
    {
        //Log.d(TAG,"onRequestPermissionsResult");
        if (code == requestCode) {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0){
                    for (int i:grantResults) {
                        if (i == PackageManager.PERMISSION_DENIED){
                            permissionsDenied();
                            return;
                        }
                    }
                    permissionsGranted();
                    return;
                } else {
                    permissionsDenied();
                }
                return;
            }
            else
                permissionsDenied();
    }

    private void permissionsGranted(){
        //Log.d(TAG,"permissionsGranted");
        UnityPlayer.UnitySendMessage(gameObjectName, CALLBACK_METHOD_NAME, Integer.toString(requestCode));
    }

    private void permissionsDenied(){
        //Log.d(TAG,"permissionsDenied");
        Toast.makeText(unityActivity.getBaseContext(),"Permission denied", Toast.LENGTH_LONG).show();
    }

}
